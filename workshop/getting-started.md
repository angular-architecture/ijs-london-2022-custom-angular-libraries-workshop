# Getting Started

## Prerequisites

- Visual Studio Code
- Node Version Manager (nvm)
- Git
- AWS Account

### Visual Studio Code

Download and install the latest version of Visual Studio Code at [https://code.visualstudio.com/download](https://code.visualstudio.com/download).

### Node

You will need to install Node version `14.15.5` on your machine. There are different ways to do this. I recommend using Node Version Manager (nvm). 

### Node Version Manager (nvm)

Install Node Version Manager (nvm) on you machine. This will allow you to target specific versions of Node and to change them without requiring a complete removal and install of Node.

- Mac OS installation: [https://tecadmin.net/install-nvm-macos-with-homebrew/](https://tecadmin.net/install-nvm-macos-with-homebrew/) for instructions.
- Max Big Sur installation: [https://andrejacobs.org/macos/installing-node-js-on-macos-big-sur-using-nvm/](https://andrejacobs.org/macos/installing-node-js-on-macos-big-sur-using-nvm/)
- Windows installation: [https://dev.to/skaytech/how-to-install-node-version-manager-nvm-for-windows-10-4nbi](https://dev.to/skaytech/how-to-install-node-version-manager-nvm-for-windows-10-4nbi)

```ts
# install nvm
nvm use 14.15.5
node -v > .nvmrc
node -v #verify version
```

> After installing nmv, use the `nvm install 14.15.5` command to install and use this specific version.

Use the `node -v` command on your terminal to verify Node exists and the version

### Angular Version

We will use the following Angular version for the workshop.

> Angular, Node TypeScript, and RxJS compatibility chart:
> See: https://gist.github.com/LayZeeDK/c822cc812f75bb07b7c55d07ba2719b3

- [@angular/cli 12.2.6](https://www.npmjs.com/package/@angular/cli/v/12.2.16)
- [node 14.15.5](https://nodejs.org/ca/blog/release/v14.15.5/)

Install Node version 14.15.5 using nmv: `nvm install 14.15.5 `

> Verify the installation with: `nvm ls`

If you already have the Angular CLI installed, you can determine what version of Angular you are using with this command: `ng version`

```ts
ng version

     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/
    

Angular CLI: 11.2.14
Node: 14.15.5
OS: darwin x64

Angular: 
... 
Ivy Workspace: 

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.1102.14 (cli-only)
@angular-devkit/core         11.2.14 (cli-only)
@angular-devkit/schematics   11.2.14 (cli-only)
@schematics/angular          11.2.14 (cli-only)
@schematics/update           0.1102.14 (cli-only)
```

Install updated version using npm: `npm install -g @angular/cli@12.2.6`. Verify the new installation and version with: `ng version`.

```ts
ng version

     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/
    

Angular CLI: 12.2.16
Node: 14.15.5
Package Manager: npm 6.14.11
OS: darwin x64

Angular: 
... 

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.1202.16 (cli-only)
@angular-devkit/core         12.2.16 (cli-only)
@angular-devkit/schematics   12.2.16 (cli-only)
@schematics/angular          12.2.16 (cli-only)
``

### Git

Use the `git --version` on your terminal to verify Git exists and the version.

```ts
git version 2.30.1 (Apple Git-130)
```

### Nx CLI

A significant portion of the workshop will take advantage of the Nx.dev tools and the Nx Workspace. Please install the following package globally on your machine. 

> npm i -g @nrwl/workspace@12.10.1

## DataDogHQ Account for Logging

One of the workshop experiments is to set up a logging service library. We will use [DataDogHQ](https://www.datadoghq.com/) as our centralized logging repository. 

- set up a new account
- verify your account with the email address provided