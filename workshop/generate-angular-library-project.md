# Generate Angular Library Project

## Overview

- use Angular CLI to create new library project
- adds project definition to the `angular.json` file

## Error Handling Library (Service Library)

```ts
ng g library error-handling --help
Generate a library project for Angular.
usage: ng generate library <name> [options]

arguments:
  schematic
    The schematic or collection:schematic to generate.
  name
    The name of the library.

options:
  --defaults 
    Disable interactive input prompts for options with a default.
  --dry-run (-d)
    Run through and reports activity without writing out results.
  --entry-file 
    The path at which to create the library's public API file, relative to the workspace root.
  --force (-f)
    Force overwriting of existing files.
  --help 
    Shows a help message for this command in the console.
  --interactive 
    Enable interactive input prompts.
  --lint-fix 
    Apply lint fixes after generating the library.
  --prefix (-p)
    A prefix to apply to generated selectors.
  --skip-install 
    Do not install dependency packages.
  --skip-package-json 
    Do not add dependencies to the "package.json" file. 
  --skip-ts-config 
    Do not update "tsconfig.json" to add a path mapping for the new library. The path mapping is needed to use the library in an app, but can be disabled here to simplify development.
```

Run the CLI command with the `--dry-run` option.

```ts
ng g library error-handling --dry-run                
CREATE projects/error-handling/README.md (1042 bytes)
CREATE projects/error-handling/karma.conf.js (1435 bytes)
CREATE projects/error-handling/ng-package.json (163 bytes)
CREATE projects/error-handling/package.json (193 bytes)
CREATE projects/error-handling/tsconfig.lib.json (405 bytes)
CREATE projects/error-handling/tsconfig.lib.prod.json (240 bytes)
CREATE projects/error-handling/tsconfig.spec.json (309 bytes)
CREATE projects/error-handling/src/public-api.ts (187 bytes)
CREATE projects/error-handling/src/test.ts (806 bytes)
CREATE projects/error-handling/src/lib/error-handling.module.ts (278 bytes)
CREATE projects/error-handling/src/lib/error-handling.component.spec.ts (676 bytes)
CREATE projects/error-handling/src/lib/error-handling.component.ts (288 bytes)
CREATE projects/error-handling/src/lib/error-handling.service.spec.ts (393 bytes)
CREATE projects/error-handling/src/lib/error-handling.service.ts (142 bytes)
UPDATE angular.json (4510 bytes)
UPDATE package.json (1110 bytes)
UPDATE tsconfig.json (915 bytes)

NOTE: The "dryRun" flag means no changes were made.
```

The output without the `--dry-run` option.

```ts
ng g library error-handling                                    
CREATE projects/error-handling/README.md (1042 bytes)
CREATE projects/error-handling/karma.conf.js (1435 bytes)
CREATE projects/error-handling/ng-package.json (163 bytes)
CREATE projects/error-handling/package.json (193 bytes)
CREATE projects/error-handling/tsconfig.lib.json (405 bytes)
CREATE projects/error-handling/tsconfig.lib.prod.json (240 bytes)
CREATE projects/error-handling/tsconfig.spec.json (309 bytes)
CREATE projects/error-handling/src/public-api.ts (187 bytes)
CREATE projects/error-handling/src/test.ts (806 bytes)
CREATE projects/error-handling/src/lib/error-handling.module.ts (278 bytes)
CREATE projects/error-handling/src/lib/error-handling.component.spec.ts (676 bytes)
CREATE projects/error-handling/src/lib/error-handling.component.ts (288 bytes)
CREATE projects/error-handling/src/lib/error-handling.service.spec.ts (393 bytes)
CREATE projects/error-handling/src/lib/error-handling.service.ts (142 bytes)
UPDATE angular.json (4510 bytes)
UPDATE package.json (1110 bytes)
UPDATE tsconfig.json (915 bytes)
✔ Packages installed successfully.
```

Build the new library project using the CLI.

> ng build error-handling

### Library Builder

The builder: 

```json
// angular.json
"builder": "@angular-devkit/build-angular:ng-packagr",
```

The CLI output is:

```ts
ng build error-handling
Building Angular Package

------------------------------------------------------------------------------
Building entry point 'error-handling'
------------------------------------------------------------------------------
⠋ Compiling with Angular sources in Ivy partial compilation mode.Compiling @angular/core : es2015 as esm2015
✔ Compiling with Angular sources in Ivy partial compilation mode.
✔ Bundling to FESM2015
✔ Bundling to UMD
✔ Writing package metadata
✔ Built error-handling

------------------------------------------------------------------------------
Built Angular Package
 - from: /Users/user/work/gitlab/ijs-london-2022-custom-angular-libraries-workshop/angular-workspace/projects/error-handling
 - to:   /Users/user/work/gitlab/ijs-london-2022-custom-angular-libraries-workshop/angular-workspace/dist/error-handling
------------------------------------------------------------------------------

Build at: 2022-03-20T18:01:56.542Z - Time: 11549ms
```

The `dist` folder contains the output of build - from the `ng-packagr`

- umd bundles
- esm2015
- fesm2015
- public-api.d.ts: default entry point for the library

```txt
.
├── bundles
│   ├── error-handling.umd.js
│   └── error-handling.umd.js.map
├── esm2015
│   ├── lib
│   │   ├── error-handling.component.js
│   │   ├── error-handling.module.js
│   │   └── error-handling.service.js
│   ├── error-handling.js
│   └── public-api.js
├── fesm2015
│   ├── error-handling.js
│   └── error-handling.js.map
├── lib
│   ├── error-handling.component.d.ts
│   ├── error-handling.module.d.ts
│   └── error-handling.service.d.ts
├── error-handling.d.ts
├── package.json
├── public-api.d.ts
└── README.md
```

## Angular Package Format

> See: https://angular.io/guide/angular-package-format
> See: https://docs.google.com/document/d/1CZC2rcpxffTDfRDs6p1cfbmKNLA6x5O-NtkJglDaBVs/preview


### Package.json

> Primary package.json, describing the package itself as well as all available entry points and code formats. This file contains the "exports" mapping used by runtimes and tools to perform module resolution. - [Angular.io](https://angular.io/guide/angular-package-format#file-layout)

```json
{
  "name": "error-handling",
  "version": "0.0.1",
  "peerDependencies": {
    "@angular/common": "^12.2.0",
    "@angular/core": "^12.2.0"
  },
  "dependencies": {
    "tslib": "^2.3.0"
  },
  "main": "bundles/error-handling.umd.js",
  "module": "fesm2015/error-handling.js",
  "es2015": "fesm2015/error-handling.js",
  "esm2015": "esm2015/error-handling.js",
  "fesm2015": "fesm2015/error-handling.js",
  "typings": "error-handling.d.ts",
  "sideEffects": false
}
```