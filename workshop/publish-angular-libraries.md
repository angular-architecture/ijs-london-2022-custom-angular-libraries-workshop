# Publish Angular Libraries

> Overview: It is as simple as 1-2-3. Create a library. Build a library.
> And, publish a library.

1. create
2. build
3. publish

## NPM Account

> Create a new NPM account at https://npmjs.com

Sign up for a new account and provide the following information.

- User name
- Email Address
- Password

## Scope Name

> Define and use a scope name that will group and organize your library packages (e.g., @myCompany, @buildmotion, @angular, @this, or @that).
> More information at: https://docs.npmjs.com/cli/v8/using-npm/scope

To publish a public scoped package, you must specify `--access public` with the initial publication. This will publish the package and set access to public as if you had run npm access public after publishing.

```ts
cd dist/libs/logging
npm publish --access public
```

## Create Library (Step 1)

> A new library project will contain a package.json file.

The library project contains its own `package.json` file. During the build process of a library, the package.json file is copied to the `dist` output folder.

A published library is required. It contains the `name` and `version` - which is required to be unique for all packages published. There is an array of other properties and meta data that the package file can contain.

```json
{
  "name": "@buildmotion/actions",
  "version": "12.2.0",
  "description": "Implement business actions with rules, validation, and business logic.",
  "repository": {
    "type": "git",
    "url": "https://github.com/buildmotion/buildmotion",
    "directory": "libs/actions"
  },
  "keywords": [
    "actions"
  ],
  "author": "matt.vaughn@buildmotion.com www.buildmotion.com",
  "license": "MIT",
  "main": "./src/index.js",
  "typings": "./src/index.d.ts",
  "peerDependencies": {
    "@buildmotion/rules-engine": "12.0.1"
  }
}
```

### Build (Step 2)

The `build` configuration in the workspace.json file for the `actions` project.

1. the output path to the `dist` folder
2. the `tsconfig.lib.json` inherits up to the root `tsconfig` configuration of the workspace.
3. the `assets`  provides the mechanism to copy the library's `package.json` to the output.

```json
"build": {
    "executor": "@nrwl/workspace:tsc",
    "outputs": ["{options.outputPath}"],
    "options": {
    "outputPath": "dist/libs/actions",
    "main": "libs/actions/src/index.ts",
    "tsConfig": "libs/actions/tsconfig.lib.json",
    "assets": ["libs/actions/*.md", "libs/actions/package.json"]
    }
},
```

## NPM Login

You will want to login to your NPM account.

1. Use the terminal, navigate to the target library in the `dist` folder.
2. Use the `npm login --scope=@buildmotion` command.
   1. enter a `Username` value.
   2. enter your `Password`.
   3. enter you `Email` address.
   4. enter your OTP for MFA.

The following shows the output of a typical login.

```ts
npm login --scope=@buildmotion
Username: buildmotion
Password: 
Email: (this IS public) matt.vaughn@buildmotion.com
npm notice Please use the one-time password (OTP) from your authenticator application
Enter one-time password from your authenticator app: 421797
Logged in as buildmotion on https://registry.npmjs.org/.
```

## NPM Publish (Step 3)

![publish-email](resources/publish-email.png)

A successful NPM publish command will provide output like below.

```ts
npm publish --access public
npm notice 
npm notice 📦  @buildmotion/actions@12.2.0
npm notice === Tarball Contents === 
npm notice 566B  src/lib/action-result.js    
npm notice 3.8kB src/lib/Action.js           
npm notice 47B   src/lib/i-action.js         
npm notice 129B  src/index.js                
npm notice 865B  package.json                
npm notice 319B  src/lib/action-result.js.map
npm notice 1.6kB src/lib/Action.js.map       
npm notice 137B  src/lib/i-action.js.map     
npm notice 208B  src/index.js.map            
npm notice 168B  README.md                   
npm notice 420B  src/lib/action-result.d.ts  
npm notice 4.7kB src/lib/Action.d.ts         
npm notice 181B  src/lib/i-action.d.ts       
npm notice 138B  src/index.d.ts              
npm notice === Tarball Details === 
npm notice name:          @buildmotion/actions                    
npm notice version:       12.2.0                                  
npm notice package size:  3.6 kB                                  
npm notice unpacked size: 13.3 kB                                 
npm notice shasum:        fcb0c311cc71bfcd7185fd475ec88aa1b80fb0a5
npm notice integrity:     sha512-Ny8fF8jwGcW9Y[...]JWlT05Mumfj1g==
npm notice total files:   14                                      
npm notice 
+ @buildmotion/actions@12.2.0
```

## Consuming Published Packages

You can add the packages to your workspace `package.json` file in the `dependencies` section.

> Run `npm install` or `yarn install` to add the packages. This will retrieve the packages from the npm registry (i.e., https://npmjs.com) and add the packages to the `node_modules` folder.

```json
"@buildmotion/core": "^12.2.0",
"@buildmotion/rules-engine": "^12.2.0",
"@buildmotion/actions": "^12.2.0",
"@buildmotion/configuration" : "^12.2.0",
"@buildmotion/logging" : "^12.2.0",
"@buildmotion/foundation" : "^12.2.0",
"@buildmotion/common" : "^12.2.0",
"@buildmotion/error-handling": "^12.2.0",
"@buildmotion/http-service": "^12.2.0",
"@buildmotion/notifications" : "^12.2.0",
"@buildmotion/validation" : "^12.2.0",
```

Or, you can run the following command(s) to add the packages to your workspace environment.

```ts
yarn add @buildmotion/core@12.2.0
yarn add @buildmotion/rules-engine@12.2.0
yarn add @buildmotion/actions@12.2.0
yarn add @buildmotion/configuration"@12.2.0
yarn add @buildmotion/logging"@12.2.0
yarn add @buildmotion/foundation"@12.2.0
yarn add @buildmotion/common"@12.2.0
yarn add @buildmotion/error-handling@12.2.0
yarn add @buildmotion/http-service@12.2.0
yarn add @buildmotion/notifications"@12.2.0
yarn add @buildmotion/validation"@12.2.0
```