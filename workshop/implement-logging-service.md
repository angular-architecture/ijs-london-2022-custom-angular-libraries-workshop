# Implement Logging Service

## Add DataDog package

```ts
yarn add @datadog/browser-logs
```

Verify the installation in your package.json
[a])
```json
"@datadog/browser-logs": "^4.7.0",
```

// FIXME: ADD CONTENT HERE

```ts
import { datadogLogs } from '@datadog/browser-logs';

datadogLogs.init({
    clientToken: '<DATADOG_CLIENT_TOKEN>',
    site: 'datadoghq.com',
    forwardErrorsToLogs: true,
    sampleRate: 100
});
```

## Add DataDog Options

Add a new class to the `logging` library. This class will allow the application to provide configuration options to the logging module.

```ts
export class DataDogOptions {
    applicationName!: string;
    clientToken!: string;
}
```

Make sure to export the `DataDogOptions` in the `public-api.ts` file. This will export the class and allow consumers of the Library to provide login configuration options.

```ts
// projects/logging/src/public-api.ts
/*
 * Public API Surface of logging
 */

export * from './lib/logging.service';
export * from './lib/logging.component';
export * from './lib/logging.module';

export { DataDogOptions } from './lib/datadog-options';
```

## Provide Configuration Options

Add a `forRoot()` method to provide the configuration options to the library service.

```ts
import { ModuleWithProviders, NgModule } from '@angular/core';
import { DataDogOptions } from './datadog-options';
import { LoggingComponent } from './logging.component';

@NgModule({
  declarations: [
    LoggingComponent
  ],
  imports: [
  ],
  exports: [
    LoggingComponent
  ]
})
export class LoggingModule {
  static forRoot(options: DataDogOptions): ModuleWithProviders<LoggingModule> {
    return {
      ngModule: LoggingModule,
      providers: [
        {
          provide: DataDogOptions,
          useValue: options
        }
      ]
    }
  }
}
```

## Initialize DataDog in Service

Perform the following tasks to initialize the DataDog logging provider.

1. update the constructor to consume the `DataDogOptions` provided in the module
2. add a call to and implement the `initializeLogger()` method.


```ts
import { Injectable } from '@angular/core';
import { datadogLogs } from '@datadog/browser-logs';
import { DataDogOptions } from './datadog-options';

@Injectable({
  providedIn: 'root'
})
export class LoggingService {

  constructor(private loggerOptions: DataDogOptions) {
    this.initializeLogger();
  }

  private initializeLogger() {
    datadogLogs.init({
      clientToken: this.loggerOptions.clientToken,
      site: 'datadoghq.com',
      forwardErrorsToLogs: true,
      sampleRate: 100
    });

    datadogLogs.logger.log(`Initialized DataDog logger.`, { application: this.loggerOptions.applicationName }, 'info');
  }
}
```

### Add Log Method

Now we want to add a log method to the login service. This will allow consumers of the login service to provide a message to the logging provider.

1. add a `log` method to the `LoggingService` class
2. Use the `datadogLogs.logger.info()` method to pass in a log message.

```ts
  log(message: string) {
    datadogLogs.logger.info(message);
  }
```

> Note: Make sure to build the library after any changes to the code.
> Use CLI command: `ng build logging`

BUILD:

```ts
ng build logging
Building Angular Package

------------------------------------------------------------------------------
Building entry point 'logging'
------------------------------------------------------------------------------
⠋ Compiling with Angular sources in Ivy partial compilation mode.Compiling @angular/core : es2015 as esm2015
✔ Compiling with Angular sources in Ivy partial compilation mode.
✔ Bundling to FESM2015
⠙ Bundling to UMDWARNING: No name was provided for external module '@datadog/browser-logs' in output.globals – guessing 'browserLogs'
✔ Bundling to UMD
✔ Writing package metadata
✔ Built logging

------------------------------------------------------------------------------
Built Angular Package
 - from: /Users/user/work/gitlab/ijs-london-2022-custom-angular-libraries-workshop/angular-workspace/projects/logging
 - to:   /Users/user/work/gitlab/ijs-london-2022-custom-angular-libraries-workshop/angular-workspace/dist/logging
------------------------------------------------------------------------------

Build at: 2022-03-26T16:06:11.268Z - Time: 15645ms
```


## Configure the Logging Service for the Application

Create a configuration using the `DataDogOptions` class. Update the application name and add your `clientToken` from DataDogHQ.

> NOTE: You can create production and non-production tokens and let the environment control 
> which one get loaded.

```ts
const loggingOptions: DataDogOptions = {
  applicationName: 'ijs-london',
  clientToken: '<ADD-YOUR-DATADOG-CLIENT-TOKEN-HERE>'
}
```

```ts

import { ErrorHandlingModule, ErrorHandlingService } from 'error-handling';
import { LoggingModule, LoggingService } from 'logging';
import { DataDogOptions } from 'projects/logging/src/public-api';

const loggingOptions: DataDogOptions = {
  applicationName: 'ijs-london',
  clientToken: '<ADD-YOUR-DATADOG-CLIENT-TOKEN-HERE>'
}
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    LoggingModule.forRoot(loggingOptions),
    BrowserModule,
    AppRoutingModule,
    ErrorHandlingModule.forRoot({
      applicationName: 'ijs-library-workshop'
    }),
  ],
  providers: [
    ErrorHandlingService,
    LoggingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

## Update ErrorHandling with Logging

1. Update the LoggingService with an `error()` method. We will use this to log errors to our provider.

```ts
error(message: string) {
    datadogLogs.logger.error(message);
}
```

2. Update the ErrorHandlingService to log errors.


```ts
import { ErrorHandler, Injectable } from '@angular/core';
import { LoggingService } from 'logging';
import { ErrorHandlingOptions } from './error-handling-options';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlingService implements ErrorHandler {
`
  constructor(
    private options: ErrorHandlingOptions,
    private logger: LoggingService
  ) { }

  handleError(error: any): void {
    this.logger.error(`${this.options.applicationName}: ${error}`);
  }
}
```

## View Log Results

> Note: Make sure to build your library projects after each change.

Serve the application. You can monitor the log items in the [Log Explorer] in DataDog.

![log-results](resources/log-results.png)