# Introduction

Tell a story about an application. 

## 24 Million Dollar Mistake

Mistakes happen, right? Do we learn from our mistakes when they do? Some of us do - those that care.

- where and how can we organize our code
  - UI/UX, business logic, data access and logic
- sharing and reusing code
  - alternatives
- business rules and validation
  - location

![Billion Dollar Mi$take](resources/billion-dollar-mistake.png)