# Review Dashboard Application

We are going to update the demo Dashboard application in the workspace. We will use a demo Angular 12 application that contains several features implemented using Angular modules with lazy-loading. 

![dashboard-app](./resources/dashboard-app.png)