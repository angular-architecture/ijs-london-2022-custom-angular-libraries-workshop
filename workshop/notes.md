# Notes

> Coupon: https://leanpub.com/angular-libraries-the-complete-guide/c/8nQNr6edi8t3

git config --global user.name "Matt Vaughn"
git config --global user.email matt.vaughn@buildmotion.com

DataDog :: Client Token
pub905382b0fa24bc2f51efa742260ef110

> [Angular Package Format](https://docs.google.com/document/d/1CZC2rcpxffTDfRDs6p1cfbmKNLA6x5O-NtkJglDaBVs/mobilebasic)
> IJS: [https://javascript-conference.com/angular/custom-angular-libraries/](https://javascript-conference.com/angular/custom-angular-libraries/)
> repo: [https://github.com/buildmotion/ijs-custom-angular-libraries.git](https://github.com/buildmotion/ijs-custom-angular-libraries.git)

Complete Guide on creating custom distributed/shared libraries for Angular Applications.

This session will guide you through the process of creating shared custom Angular Libraries. You will learn how to identify candidates for a distributed library and to create different kinds of Angular libraries, like: feature, service, and component libraries.

You will learn strategies for organizing your code using Angular libraries and how to distribute and publish to NPM or in your local development environment:

- Details on different Angular Module/Library types within an application: service, feature, core, and shared.
- How to structure application for code reuse.
- Managing Custom Libraries versions.
- Details on how to publish locally and to NPM.

## Embed

```html
<iframe width='160' height='400' src='https://leanpub.com/angular-libraries-the-complete-guide/embed' frameborder='0' allowtransparency='true'></iframe>
```

## Show the How with Examples

> Just do not show the cake -show how it was made. Step-by-step flow of one of the items/workflows to demonstrate the process (65% of US are visual learner, need to see information in order to retain it)
>
> - Most people process information based on what they see

## Step-by-Step vs. The Science Behind

> Explain the principles behind the step-by-step instructions

- knowing
- understanding
- application/execution (doing)
  - practice
  - repetition of the right things
  - muscle-memory

### Explain the Why

## Story

- had to build (6) Angular applications within a 1-year period
  - dashboard reporting and analytics
  - device management
  - device testing and metrics
  - video training system
  - license adminstration tool
  - client license server
- team
  - small, new members, new domain
- data
  - about 150 database tables, pristine database and schema(s)
- integrate and work with medical devices
- all applications had to work on delivery - no exceptions
  - install and configure at client clinics and hospitals