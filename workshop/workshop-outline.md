# IJS Custom Library Workshop Outline

- create and share libraries
- how to distribute and publish libraries
- manage library versions
- code organization strategies using libraries
- identify library candidates

## Overview

This workshop will guide you through the process of creating shared custom Angular Libraries. You will learn how to identify candidates for a distributed library and to create different kinds of Angular libraries, like feature, service, and component libraries. You will learn strategies for organizing your code using Angular libraries and how to distribute and publish to NPM or in your local development environment.

### Content & Process

The following additional topics will be covered in detail in the workshop:

- Details on different Angular Module/Library types within an application: service, feature, core, and shared
- How to structure applications for code reuse
- Managing custom library versions
- Details on how to publish locally and to NPM

After visiting this workshop, you’ll have a complete guide on creating custom distributed and shared libraries for your Angular applications.

### Audience & Requirements
This workshop is an intermediate/advanced level.

Please make sure your computer/laptop has the following installed to join the workshop:

- Visual Studio Code (latest)
- Node (version 14.15.5)
- Git (latest)

## Introduction (15m)

  - [ ] What is a library?

## What is an Angular Library? (30m)

  - [ ] Project Type
  - [ ] May 4, 2018 (Angular Version 6)
  - [ ] angular.json
    - [ ] manifest of application and library projects
  - [ ] Angular CLI
    - [ ] `ng new NAME_GOES_HERE`

## Generate an Angular Application

## Generate an Angular Library
## Difference between a Package and a Library (15m)

  - [ ] Build
  - [ ] Deploy
  - [ ] Distribution
  - [ ] Consume

## What capabilities do library provide? (1.25)

- [ ] Code Organization
  - [ ] Feature (Vertical)
  - [ ] Responsibility (Horizontal)
  - [ ] Cross-Cutting
- [ ] Encapsulation
  - [ ] Implementation Details
  - [ ] API/Behaviors
- [ ] Lazy-Loading Features
  - [ ] Modules
- [ ] Architecture
  - [ ] Enable
  - [ ] Enforcement
  - [ ] Layered
  - [ ] n-tier
  - [ ] Division of Responsibilities/Concerns
- [ ] Code Duplication
  - [ ] Copy/Paste
  - [ ] Similar Code
  - [ ] Snowflake API Calls
- [ ] Consistency
  - [ ] Single-Source of Truth
- [ ] Maintainability
  - [ ] Location of Code
- [ ] Testability
  - [ ] Focused
- [ ] Quality
  - [ ] 

## Angular Library Types (1 hr)

  - [ ] Feature
    - [ ] Products
    - [ ] Orders
    - [ ] Shipments
  - [ ] Domain Service
    - [ ] Products
    - [ ] Orders
    - [ ] Shipments
  - [ ] Component
    - [ ] Directives
    - [ ] Pipes
    - [ ] Notifiers
  - [ ] Utilities
      - [ ] Validation
  - [ ] Cross-Cutting Concerns
    - [ ] Configuration
    - [ ] Logging
    - [ ] Error Handling
    - [ ] HTTP Service
    - [ ] Notification
  - [ ] Frameworks
    - [ ] Actions
    - [ ] Rules Engine
    - [ ] State Machine

## Identify Library Candidates (15m)

- [ ] Refactor Path
- [ ] Similar Code
- [ ] Features
  - [ ] Domain Specific
- [ ] Non-Feature
  - [ ] Functional, Utilities, Cross-Cutting concerns

## Path to Library

- [ ] Identify Candidates
- [ ] Isolate Code as Modules
  - [ ] Abstract
  - [ ] Encapsulate

## Creating Angular Libraries (1h)

  - [ ] Workspace Differences
    - [ ] Angular CLI/Workspace
    - [ ] Nx CLI/Workspace

## Using Libraries in Angular Workspace (30m)

## Using Libraries in Nx Workspace (1h)

## Building Libraries (30m)

- [ ] Dependencies
- [ ] Peer Dependencies
- [ ] Package Information

## Publishing Libraries (30m)

- [ ] Private
- [ ] Public
- [ ] Documentation

## Code Organization Strategies (1h)
- [ ] Feature Verticals
- [ ] Layers (Horizontal)
- [ ] Architecture
- [ ] Layouts as a Library (LaaL)
- [ ] Assets as a Library (AaaL)
- [ ] Feature as a Library (FaaL)
  - [ ] Components
  - [ ] UI Service/Adaptor
- [ ] Domain Service - Business Logic as a Library (BLaaL)
  - [ ] Domain API Service
  - [ ] Business Logic
  - [ ] Business Actions
    - [ ] Business Rules
    - [ ] Validation
  - [ ] Domain Repository
    - [ ] Data Access
    - [ ] HTTP/API Operations