# Implement Error Handling Library

## Angular Core ErrorHandler

- Note the comments in the default Amgular ErrorHandler
  - writes logs to the console.

```ts
/**
 * Provides a hook for centralized exception handling.
 *
 * The default implementation of `ErrorHandler` prints error messages to the `console`. To
 * intercept error handling, write a custom exception handler that replaces this default as
 * appropriate for your app.
 *
 * @usageNotes
 * ### Example
 *
 * ```
 * class MyErrorHandler implements ErrorHandler {
 *   handleError(error) {
 *     // do something with the exception
 *   }
 * }
 *
 * @NgModule({
 *   providers: [{provide: ErrorHandler, useClass: MyErrorHandler}]
 * })
 * class MyModule {}
 * ```
 *
 * @publicApi
 */
export declare class ErrorHandler {
    handleError(error: any): void;
}
```

## Implement ErrorHandler Interface

- implement the `handleError()` method of the ErrorHandler interface.
  - provides a customized handling of the error
- write to a centralized repository (e.g., DataDogHQ, Loggly, AWS, etc.)

```ts
import { ErrorHandler, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlingService implements ErrorHandler {

  constructor() { }

  handleError(error: any): void {
    console.error(error);
  }
}
```

## Add Options (Configuration) to Lib

Create a class for the configuration.

- allows for providing configuration options to the service
- using Angular Dependency Injection and the `forRoot()` convention

```ts
export class ErrorHandlingOptions {
    applicationName!: string;
}
```

Update the module with a *static* `forRoot()` method.

- static method to provide information
- providers (injection) scoped at the provider level
- options class

```ts
import { NgModule, ModuleWithProviders } from '@angular/core';
import { ErrorHandlingOptions } from './error-handling-options';

@NgModule({declarations: [], imports: [],exports: []
})
export class ErrorHandlingModule {
  static forRoot(options: ErrorHandlingOptions): ModuleWithProviders<ErrorHandlingModule> {
    return {
      ngModule: ErrorHandlingModule,
      providers: [
        // add providers here;
        {
          provide: ErrorHandlingOptions,
          useValue: options
        }
      ]
    }
  }
}
```

### Use the Options in the Service

- `private` injector of options
- options are provided and available via DI in the constructor

```ts
import { ErrorHandler, Injectable } from '@angular/core';
import { ErrorHandlingOptions } from './error-handling-options';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlingService implements ErrorHandler {

  constructor(private options: ErrorHandlingOptions) { }

  handleError(error: any): void {
    console.error(`${this.options.applicationName}: ${error}`);
  }
}
```

## Export Options

Allows the `ErrorHandlingOptions` to be used outside of the library project.

- add the options/config class to the barrel file of the library projects.
- exports the class and exposes it to consumers.

```ts
/*
 * Public API Surface of error-handling
 */

export { ErrorHandlingService } from './lib/error-handling.service';
export { ErrorHandlingModule } from './lib/error-handling.module';
export { ErrorHandlingOptions } from './lib/error-handling-options';
```

## Import Paths for tsconfig

A `paths` attribute is used in the `compilerOptions` section in the `tsconfig.json` file.

- mapping a path to an alias (i.e., error-handling) of our library
- points to the output directory of the build for the project

```json
"paths": {
      "error-handling": [
        "dist/error-handling/error-handling",
        "dist/error-handling"
      ]
    },
```

## Provide the Service to the Application

- import the module
- use the `forRoot()` method to provide the options
- add the `ErrorHandlingService` to the `providers` collection

```ts
import { ErrorHandlingModule, ErrorHandlingService } from 'error-handling';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ErrorHandlingModule.forRoot({
      applicationName: 'ijs-library-workshop'
    })
  ],
  providers: [
    ErrorHandlingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

## Use the ErrorHandlingService

> Example only. This sample is to demonstrate the service is available
> for the application.

- inject the service into the constructor
- call a method on the service

```ts
import { Component } from '@angular/core';
import { ErrorHandlingService } from 'error-handling';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ijs';

  constructor(private errorService: ErrorHandlingService) {
    this.errorService.handleError(new Error('...hello error'));
  }
}
```

### Build and Serve

Run the CLI commands.

```ts
ng build error-handling
ng serve
```

### Add a Launch Configuration

Add a launch.json configuration file.

- click on the *Run and Debug* icon
- click the option "create a launch.json" file
- update the port to `4200`

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "type": "pwa-chrome",
            "request": "launch",
            "name": "Launch Chrome against localhost",
            "url": "http://localhost:4200",
            "webRoot": "${workspaceFolder}"
        }
    ]
}
```

### Add a Debug Breakpoint 

![debug-service](resources/debug-service.png)

### Launch Application

- F5
- click the "play" button in the [Run and Debug]

![run-and-debug](resources/run-and-debug.png)

- options injected into the constructor
- available for use within the service

![debug](resources/debug.png)

- serve the application
- monitor the dev tools and console log
- view errors

> Verifying the application is using the custom ErrorHandler.

![devtools-console](resources/devtools-console.png)