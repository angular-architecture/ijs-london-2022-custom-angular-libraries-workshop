# Use Error Handling Library

The library project can be used or consumed by other library projects and as many application projects. This library project is considered a `service` library. This is just one of many different types of library projects.

## Import Paths for tsconfig

Add a `paths` attribute to the `angularCompilerOptions` section in the `tsconfig.json` file.

- mapping a path to an alias (i.e., error-handling) of our library

```json
"paths": {
    "error-handling": [
      "dist/error-handling"
    ]
  }
```

## Provide the Service to the Application

- import the module
- use the `forRoot()` method to provide the options
- add the `ErrorHandlingService` to the `providers` collection

```ts
import { ErrorHandlingModule, ErrorHandlingService } from 'error-handling';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ErrorHandlingModule.forRoot({
      applicationName: 'ijs-library-workshop'
    })
  ],
  providers: [
    ErrorHandlingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

## Use the ErrorHandlingService

> Example only. This sample is to demonstrate the service is available
> for the application.

- inject the service into the constructor
- call a method on the service

```ts
import { Component } from '@angular/core';
import { ErrorHandlingService } from 'error-handling';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ijs';

  constructor(private errorService: ErrorHandlingService) {
    this.errorService.handleError(new Error('...hello error'));
  }
}
```

### Build and Serve

Run the CLI commands.

```ts
ng build error-handling
ng serve
```

### Add a Launch Configuration

Add a launch.json configuration file.

- click on the *Run and Debug* icon
- click the option "create a launch.json" file
- update the port to `4200`

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "type": "pwa-chrome",
            "request": "launch",
            "name": "Launch Chrome against localhost",
            "url": "http://localhost:4200",
            "webRoot": "${workspaceFolder}"
        }
    ]
}
```

### Add a Debug Breakpoint 

![debug-service](resources/debug-service.png)

### Launch Application

- F5
- click the "play" button in the [Run and Debug]

![run-and-debug](resources/run-and-debug.png)

asdf

![debug](resources/debug.png)

asdf

![devtools-console](resources/devtools-console.png)