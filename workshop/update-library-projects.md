# Update Library Projects

## Update Logging Project

Use the `LoggingService` implementation from the *angular-workspace* from the earlier workshop.

Update the barrel file with the `DataDogOptions` export.

```ts
// libs/logging/src/index.ts
export * from './lib/logging.module';

export { LoggingService } from './lib/logging.service';
export { DataDogOptions } from './lib/datadog-options';
```

Update the workspace root `tsconfig.base.json` paths for the new library projects.

```json
// tsconfig.base.json
"paths": {
    "@ijs-london/logging": [
    "libs/logging/src/index.ts"
    ],
    "@ijs-london/error-handling": ["libs/error-handling/src/index.ts"]
}
```

## Add DataDog Package

```ts
yarn add @datadog/browser-logs -S
```

## Add Error Handling Project

Use the Nx CLI to create a new `error-handling` library project. Use the options:

- `--publishable`
- `--simpleModuleName`
- `--importPath`

```ts
nx g @nrwl/angular:library error-handling --publishable --simpleModuleName --importPath=@ijs-london/error-handling  
UPDATE workspace.json
UPDATE nx.json
CREATE libs/error-handling/README.md
CREATE libs/error-handling/ng-package.json
CREATE libs/error-handling/package.json
CREATE libs/error-handling/tsconfig.lib.json
CREATE libs/error-handling/tsconfig.lib.prod.json
CREATE libs/error-handling/tsconfig.spec.json
CREATE libs/error-handling/src/index.ts
CREATE libs/error-handling/src/lib/error-handling.module.ts
CREATE libs/error-handling/tsconfig.json
UPDATE tsconfig.base.json
CREATE libs/error-handling/jest.config.js
CREATE libs/error-handling/src/test-setup.ts
CREATE libs/error-handling/.eslintrc.json
```

## Use Libraries in Dashboard Application

Update the `AppModule` for the dashboard application.

```ts
const loggingOptions: DataDogOptions = {
  applicationName: 'ijs-london',
  clientToken: 'pub905382b0fa24bc2f51efa742260ef110'
}
```

Import the `ErrorHandlingModule` with the `forRoot()` method.

```ts
ErrorHandlingModule.forRoot({
    applicationName: 'ijs-library-workshop'
}),
```

Import the LoggingModule with the `forRoot()` method.

```ts
LoggingModule.forRoot(loggingOptions),
```

### AppModule

```ts
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ErrorHandlingModule, ErrorHandlingService } from 'error-handling';
import { LoggingModule, LoggingService, DataDogOptions } from 'logging';

const loggingOptions: DataDogOptions = {
  applicationName: 'ijs-london',
  clientToken: 'pub905382b0fa24bc2f51efa742260ef110'
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ErrorHandlingModule.forRoot({
      applicationName: 'ijs-library-workshop'
    }),
    LoggingModule.forRoot(loggingOptions),
  ],
  providers: [
    ErrorHandlingService,
    LoggingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
```