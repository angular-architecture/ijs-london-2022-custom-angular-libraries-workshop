# Create Logging Library

```ts
ng g library Logging
```

 Here is the output of the CLI to create a new library project for logging.

```ts
ng g library logging   
CREATE projects/logging/README.md (987 bytes)
CREATE projects/logging/karma.conf.js (1428 bytes)
CREATE projects/logging/ng-package.json (156 bytes)
CREATE projects/logging/package.json (186 bytes)
CREATE projects/logging/tsconfig.lib.json (405 bytes)
CREATE projects/logging/tsconfig.lib.prod.json (240 bytes)
CREATE projects/logging/tsconfig.spec.json (309 bytes)
CREATE projects/logging/src/public-api.ts (159 bytes)
CREATE projects/logging/src/test.ts (806 bytes)
CREATE projects/logging/src/lib/logging.module.ts (247 bytes)
CREATE projects/logging/src/lib/logging.component.spec.ts (633 bytes)
CREATE projects/logging/src/lib/logging.component.ts (268 bytes)
CREATE projects/logging/src/lib/logging.service.spec.ts (362 bytes)
CREATE projects/logging/src/lib/logging.service.ts (136 bytes)
UPDATE angular.json (5490 bytes)
UPDATE tsconfig.json (1085 bytes)
✔ Packages installed successfully.
```

## Build the Library

```ts
ng build logging
```

 Here is the output of the build for the logging library.

```txt
.
├── bundles
│   ├── logging.umd.js
│   └── logging.umd.js.map
├── esm2015
│   ├── lib
│   │   ├── logging.component.js
│   │   ├── logging.module.js
│   │   └── logging.service.js
│   ├── logging.js
│   └── public-api.js
├── fesm2015
│   ├── logging.js
│   └── logging.js.map
├── lib
│   ├── logging.component.d.ts
│   ├── logging.module.d.ts
│   └── logging.service.d.ts
├── logging.d.ts
├── package.json
├── public-api.d.ts
└── README.md
```


