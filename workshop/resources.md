# Resources

- [Angular.io: Creating Libraries](https://Angular.io/guide/creating-libraries)
- [Angular.io: Angular Library Project Files](https://angular.io/guide/file-structure#library-project-files)
- [Angular.io: Workspace](https://angular.io/guide/glossary#workspace)
- [Building libraries with Ivy](https://angular.io/guide/creating-libraries#building-libraries-with-ivy)