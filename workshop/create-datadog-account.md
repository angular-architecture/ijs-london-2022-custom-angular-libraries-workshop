# Create DataDog Account

> Use for centralized logging of application events.

![datadog-signup](resources/datadog-signup.png)

1. create a new account (free/no credit card required)
2. skip adding extra information
3. click the dog icon in the left navigation
4. select the [Logs | Getting Started] link from the navigation pane

![logs-getting-started](resources/logs-getting-started.png)

Configure the Client

1. select the [Client...JavaScript] option as the source for your logs

![client-javascript](resources/client-javascript.png)

2. select the [JS] option

![js-option](resources/js-option.png)

3. retrieve your client token

![retrieve-client-token](resources/retrieve-client-token.png)

> Use the [Client Tokens...] option.

![client-tokens](resources/client-token-link.png)

4. Click the [New Client Token] button

![client token button](resources/new-client-token-button.png)

5. Add token name

![add token name](resources/add-token-name.png)

6. Copy your token