# Nx Workspace

## Create New Workspace

 Using the Nx CLI, create a new workspace using the ***latest version minus one*** of the Nrwl package. To target a specific package version you want to go to [npmjs.com](npmjs.com) to determine the specific version number you want to use.

> https://www.npmjs.com/package/@nrwl/angular

We will target version 12 for our new workspace. We want to use the latest minor/patch release of this major version 12 (e.g., 12.10.1). Use the *npx* command to create an empty workspace for our projects. Now we can add the version number to our `npx` command to specify the version we want.

> We will use the `--npm-scope` option with a value of `ijs-london` to group and organize our custom libraries.

```ts
npx create-nx-workspace@12.10.1 nx-workspace --npm-scope=ijs-london --help -d
```

Examine the option to determine which ones you need to use.

| Option         | Description                                                                                                                                                                                                                   |
| -------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| name           | Workspace name (e.g., org name)                                                                                                                                                                                               |
| preset         | What to create in a new workspace (options: "empty", "npm", "web-components", "angular", "angular-nest", "react", "react-express", "react-native", "next", "gatsby", "nest", "express")                                       |
| appName        | The name of the application created by some presets                                                                                                                                                                           |
| cli            | CLI to power the Nx workspace (options: "nx", "angular")                                                                                                                                                                      |
| style          | Default style option to be used when a non-empty preset is selected: options: ("css", "scss", "less") plus ("styl") for all non-Angular and ("styled-components", "@emotion/styled", "styled-jsx") for React, Next.js, Gatsby |
| interactive    | Enable interactive mode when using presets (boolean)                                                                                                                                                                          |
| packageManager | Package manager to use (npm, yarn, pnpm)                                                                                                                                                                                      |
| nx-cloud       | Use Nx Cloud (boolean)                                                                                                                                                                                                        |


Use the following for our workshop workspace.

```ts
npx create-nx-workspace@12.10.1 nx-workspace --npm-scope=ijs-london --preset=empty --packageManager=yarn nx-cloud=false
```

The output should be similar:

```ts
npx create-nx-workspace@12.10.1 nx-workspace --npm-scope=ijs-london --preset=empty --packageManager=yarn nx-cloud=false           
 
npx: installed 48 in 4.974s
✔ Use Nx Cloud? (It's free and doesn't require registration.) · No

>  NX  Nx is creating your workspace.

  To make sure the command works reliably in all environments, and that the preset is applied correctly,
  Nx will run "yarn install" several times. Please wait.

✔ Installing dependencies with yarn
✔ Nx has successfully created the workspace.
```

## Nx Workspace Structure

- multi-project environment
- shared package.json
- shared/inherited configurations
- tools
  - specification tests
  - E2# tests
- Nx CLI

### package.json

```json
{
  "name": "workspace",
  "version": "0.0.0",
  "license": "MIT",
  "scripts": {
    "nx": "nx",
    "start": "nx serve",
    "build": "nx build",
    "test": "nx test",
    "lint": "nx workspace-lint && nx lint",
    "e2e": "nx e2e",
    "affected:apps": "nx affected:apps",
    "affected:libs": "nx affected:libs",
    "affected:build": "nx affected:build",
    "affected:e2e": "nx affected:e2e",
    "affected:test": "nx affected:test",
    "affected:lint": "nx affected:lint",
    "affected:dep-graph": "nx affected:dep-graph",
    "affected": "nx affected",
    "format": "nx format:write",
    "format:write": "nx format:write",
    "format:check": "nx format:check",
    "update": "nx migrate latest",
    "workspace-generator": "nx workspace-generator",
    "dep-graph": "nx dep-graph",
    "help": "nx help"
  },
  "private": true,
  "dependencies": {
    "tslib": "^2.0.0"
  },
  "devDependencies": {
    "@nrwl/tao": "12.10.1",
    "@nrwl/cli": "12.10.1",
    "@nrwl/workspace": "12.10.1",
    "@types/node": "14.14.33",
    "dotenv": "8.2.0",
    "ts-node": "~9.1.1",
    "typescript": "~4.0.3",
    "prettier": "2.2.1"
  }
}
```

### workspace.json

```json
{
  "version": 2,
  "projects": {},
  "cli": {
    "defaultCollection": "@nrwl/workspace"
  }
}
```

### TypeScript Configuration

```json
// workspace/tsconfig.base.json (b16f50f)
{
  "compileOnSave": false,
  "compilerOptions": {
    "rootDir": ".",
    "sourceMap": true,
    "declaration": false,
    "moduleResolution": "node",
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true,
    "importHelpers": true,
    "target": "es2015",
    "module": "esnext",
    "lib": ["es2017", "dom"],
    "skipLibCheck": true,
    "skipDefaultLibCheck": true,
    "baseUrl": ".",
    "paths": {}
  },
  "exclude": ["node_modules", "tmp"]
}
```

### nx.json

```json
// nx.json
{
  "npmScope": "buildmotion",
  "affected": {
    "defaultBase": "master"
  },
  "implicitDependencies": {
    "workspace.json": "*",
    "package.json": {
      "dependencies": "*",
      "devDependencies": "*"
    },
    "tsconfig.base.json": "*",
    "tslint.json": "*",
    ".eslintrc.json": "*",
    "nx.json": "*"
  },
  "tasksRunnerOptions": {
    "default": {
      "runner": "@nrwl/workspace/tasks-runners/default",
      "options": {
        "cacheableOperations": [
          "build",
          "lint",
          "test",
          "e2e"
        ]
      }
    }
  },
  "projects": {}
}
```

### .prettierrc

```json
// .prettierrc
{
  "singleQuote": true
}
```

## Add Angular Application

> FINAL: git switch part-05/section-2/add-application-project-with-options

Add the `@nrwl/angular@12.10.1` package to the new nx-workspace. Remember that the workspace is a monorepo and has the capability of multiple projects of different types. This is a short list.

- Angular
  - application
  - library
- NestJS
  - application
  - library
- Node
  - library

```ts
yarn add @nrwl/angular@12.10.1 -S`
```

You can verify the installation in the package.json file in the `dependencies` section. 

```json
"dependencies": {
  "@nrwl/angular": "12.10.1"
},
```

### Application Options

Use the `--help` option to view the options for the CLI command

>  npx create-nx-workspace@12.10.1 nx-workspace --npm-scope=ijs-london --help -d

You can use the options to fine tune the command or use it in some automation script or playbook for your team.

![dependencies](resources/nx-gen-app.png)

### New Application Project

Remove the `--help` and the `-d` or `--dry-run` options to generate the application project. The command will create a new application project with a Cypress test project as a bonus. The project definitions will be added to the `workspace.json` file.

```ts
nx generate @nrwl/angular:application dashboard --style=scss --routing 
```

### Launch and Debug the Application

Verify the application by running `nx serve`. This will build and serve the default application. 

After the application builds and is ready to view on https://localhost:4200 - you can click the `f5` key to launch the application. Update the `launch.json` file to use the correct port number of `4200`.

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "type": "pwa-chrome",
            "request": "launch",
            "name": "Launch Chrome against localhost",
            "url": "http://localhost:4200",
            "webRoot": "${workspaceFolder}"
        }
    ]
}
```

Visual Studio Code will launch and attach the running application (Chrome) to the debugger in the IDE. You should see the following:

![nx-dashboard-app](resources/nx-dashboard-app.png).

You can update the `app.component` with a constructor and log statement.

```ts
import { Component } from '@angular/core';

@Component({
  selector: 'ijs-london-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'dashboard';

  constructor() {
    console.log('We are debugging now...');
  }
}

```

The debugger will attach 

1. serve the application with `nx serve`
2. add the code and create a breakpoint
3. launch

![service-launch-debug](resources/service-launch-debug.png)

## Add Angular Library Project

You can install the Visual Studio Code extension for *Nx Console*. A visual way to use the generators. The generator script from the tool is:

```ts
node_modules/.bin/nx generate @nrwl/angular:library --name=logging --importPath=@ijs-london --publishable --no-interactive --dry-run
```

Or, you can use the Nx CLI command:

```ts
nx generate @nrwl/angular:library --name=logging --importPath=@ijs-london --publishable --no-interactive
```

### Build

> `nx build logging`

Use the `nx build` command to build the new library. The builder will use a different package.

The CLI updates the package.json with `ng-packagr" - this will provide the tooling to create the build output. It uses the Angular Package Format specification to create the entry points for different consumers of the library. 

```json
"ng-packagr": "^12.1.1",
```

You can use the CLI command to build the logging library: `nx run logging:build:production|development` or `nx build logging --configuration=production|development`.

```ts
nx run logging:build:production 
Building Angular Package

------------------------------------------------------------------------------
Building entry point '@ijs-london'
------------------------------------------------------------------------------
✔ Compiling with Angular sources in Ivy partial compilation mode.
✔ Bundling to FESM2015
✔ Bundling to UMD
✔ Writing package metadata
✔ Built @ijs-london

------------------------------------------------------------------------------
Built Angular Package
 - from: /Users/user/work/gitlab/ijs-london-2022-custom-angular-libraries-workshop/nx-workspace/libs/logging
 - to:   /Users/user/work/gitlab/ijs-london-2022-custom-angular-libraries-workshop/nx-workspace/dist/libs/logging
------------------------------------------------------------------------------
Build at: 2022-04-03T20:33:24.442Z - Time: 3978ms
———————————————————————————————————————————————

>  NX   SUCCESS  Running target "build" succeeded
✨  Done in 8.38s.
```

The output of the build is put into the `dist/libs/logging` folder. This is where you would begin your publishing of the library to an NPM registry if you need to share the library outside of the Nx Workspace.

```txt
.
├── src
│   ├── lib
│   │   └── logging.module.ts
│   ├── index.ts
│   └── test-setup.ts
├── .eslintrc.json
├── jest.config.js
├── ng-package.json
├── package.json
├── README.md
├── tsconfig.json
├── tsconfig.lib.json
├── tsconfig.lib.prod.json
└── tsconfig.spec.json
```

### Logging Workspace Configuration

The *logging* project is defined in the workspace.json file with targets to build, test, and lint.

```json
"logging": {
  "projectType": "library",
  "root": "libs/logging",
  "sourceRoot": "libs/logging/src",
  "prefix": "ijs-london",
  "targets": {
    "build": {
      "executor": "@nrwl/angular:package",
      "outputs": ["dist/libs/logging"],
      "options": {
        "project": "libs/logging/ng-package.json"
      },
      "configurations": {
        "production": {
          "tsConfig": "libs/logging/tsconfig.lib.prod.json"
        },
        "development": {
          "tsConfig": "libs/logging/tsconfig.lib.json"
        }
      },
      "defaultConfiguration": "production"
    },
    "test": {
      "executor": "@nrwl/jest:jest",
      "outputs": ["coverage/libs/logging"],
      "options": {
        "jestConfig": "libs/logging/jest.config.js",
        "passWithNoTests": true
      }
    },
    "lint": {
      "executor": "@nrwl/linter:eslint",
      "options": {
        "lintFilePatterns": [
          "libs/logging/src/**/*.ts",
          "libs/logging/src/**/*.html"
        ]
      }
    }
  }
}
```

### Logging Project Files

There are some difference between the Angular and Nx Workspace library projects.

- Nx is empty
  - Angular contains a component and service file.
- Nx uses ESLint
- Nx has *inheritance* for the TypeScript configuration files.

```txt
.
├── src
│   ├── lib
│   │   └── logging.module.ts
│   ├── index.ts
│   └── test-setup.ts
├── .eslintrc.json
├── jest.config.js
├── ng-package.json
├── package.json
├── README.md
├── tsconfig.json
├── tsconfig.lib.json
├── tsconfig.lib.prod.json
└── tsconfig.spec.json
```

### Add Service to Logging Project

Use the Nx CLI to generate a new service called *logging* to the logging project.

```ts
nx g @nrwl/angular:service logging --project=logging -d
CREATE libs/logging/src/lib/logging.service.spec.ts
CREATE libs/logging/src/lib/logging.service.ts

NOTE: The "dryRun" flag means no changes were made.
```

Remember to update the barrel file (i.e., index.ts) with the export information for the service. This will allow consumers of the library to access and use the `LoggingService`.

```ts
export * from './lib/logging.module';
export { LoggingService } from './lib/logging.service';
```

### LoggingService Scope Name

The Nx Workspace uses the NPM scope. This is a better experience when importing custom libraries. This is different from the Angular workspace which doesn't support MPM scopes in the paths configuration - it gets confused with packages that are expected to be in the node modules folder. The updated TypeScript configuration (i.e., `tsconfig.base.json`) file allows you to use the alias name with NPM scopes.

```json
"paths": {
  "@ijs-london": ["libs/logging/src/index.ts"]
}
```

### Provide the Service

We can provide the `LoggingService` in the `AppModule` file. This will create a single instance of the `LoggingService` for the application.

> Note: When the Angular application initializes, all of the services provided will be singletons and will be globally available to all consumers of the services. This only happens here when the `AppModule` is loaded.

```ts
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { LoggingService } from '@ijs-london';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot([], { initialNavigation: 'enabledBlocking' }),
  ],
  providers: [
    LoggingService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
```

### Inject and Use the LoggingService

Update the app.component and add the `private loggingService: LoggingService` in the constructor. Notice that you can use the import with the `@ijs-long` NPM scope and reference the LoggingService in the import.

```ts
import { Component } from '@angular/core';
import { LoggingService } from '@ijs-london';

@Component({
  selector: 'ijs-london-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'dashboard';

  /**
   *
   */
  constructor(
    private loggingService: LoggingService
  ) {
    // FIXME: DO NOT USE console.log in code, right?
    // console.log('We are debugging now...');
  }
}
```

You can now add the use of the login service in the constructor.

```ts
  constructor(
    private loggingService: LoggingService
  ) {
    // FIXME: DO NOT USE console.log in code, right?
    // console.log('We are debugging now...');

  }
}
```

Add a log method to the login service and send in a message to log out.

```ts
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggingService {


  constructor() { }

  log(message: string) {
    console.log(message);
  }
}
```