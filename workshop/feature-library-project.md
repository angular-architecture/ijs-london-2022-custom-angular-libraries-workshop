# Feature Library Project

## Listings

![manage-listings](resources/manage-listings.png)

### What are we going to do?

We are going to take a feature from the application and create a feature library. 

- extract the *Manage Listings* section of the Dashboard page
- create a feature library for this section
- add a route to lazy-load the new library (e.g., module)
  - target the default view *Manage Listings*
  - move the template (HTML) to the new library
- create a data model for the information used for a *Listing*

### Why do we want to do this?

- This will encapsulate the implementation and details for property listings.
- Organizes the code - isolated in a library project
- The display components may be shared with multiple application projects.
- Feature is lazy-loaded when accessed and invoked by a route - performance.
- If the feature does well, it could be a candidate for Micro Frontend application.

## Create a Feature Library with Nx CLI

- organize feature libraries in a *dashboard* folder
- use the npm-scope `@ijs-london` to group your packages; imports are more explicit
- use `--lazy` option to indicate a lazy-load of the module/route is created

```ts
# create UI lib for [listings]
nx generate @nrwl/angular:library --name=listings --style=scss --directory=dashboard --importPath=@ijs-london/dashboard/listings --lazy --linter=eslint --routing --simpleModuleName
```

The output:

```ts
nx generate @nrwl/angular:library --name=listings --style=scss --directory=dashboard --importPath=@ijs-london/dashboard/listings --lazy --linter=eslint --routing --simpleModuleName --dry-run

UPDATE workspace.json
UPDATE nx.json
CREATE libs/dashboard/listings/README.md
CREATE libs/dashboard/listings/tsconfig.lib.json
CREATE libs/dashboard/listings/tsconfig.spec.json
CREATE libs/dashboard/listings/src/index.ts
CREATE libs/dashboard/listings/tsconfig.json
CREATE libs/dashboard/listings/src/lib/listings.module.ts
UPDATE tsconfig.base.json
CREATE libs/dashboard/listings/jest.config.js
CREATE libs/dashboard/listings/src/test-setup.ts
CREATE libs/dashboard/listings/.eslintrc.json

NOTE: The "dryRun" flag means no changes were made.
```

### Workspace Project

```json
"dashboard-listings": {
    "projectType": "library",
    "root": "libs/dashboard/listings",
    "sourceRoot": "libs/dashboard/listings/src",
    "prefix": "ijs-london",
    "targets": {
    "test": {
        "executor": "@nrwl/jest:jest",
        "outputs": ["coverage/libs/dashboard/listings"],
        "options": {
        "jestConfig": "libs/dashboard/listings/jest.config.js",
        "passWithNoTests": true
        }
    },
    "lint": {
        "executor": "@nrwl/linter:eslint",
        "options": {
        "lintFilePatterns": [
            "libs/dashboard/listings/src/**/*.ts",
            "libs/dashboard/listings/src/**/*.html"
        ]
        }
    }
    }
},
```

### TypeScript Configuration 

```json
// tsconfig.base.json
"paths": {
    "@ijs-london/dashboard/listings": [
    "libs/dashboard/listings/src/index.ts"
    ],
    "@ijs-london/error-handling": ["libs/error-handling/src/index.ts"],
    "@ijs-london/logging": ["libs/logging/src/index.ts"]
}
```

### Listings Module

The ListingsModule is an entry module for the listings feature. What we want to within this feature will be implemented in specific feature components - we'll create those later/next.

```ts
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      /* {path: '', pathMatch: 'full', component: InsertYourComponentHere} */
    ]),
  ],
})
export class ListingsModule {}
```

## Add Feature Component Modules

- we would like to create all of the components below to allow the team to build out the new features.

```
# add top-level/entry component(s) for [listings]
nx g @nrwl/angular:module --name=manage-listings --project=dashboard-listings --module=/listings.module --routing --route=manage
nx g @nrwl/angular:module --name=add-listing     --project=dashboard-listings --module=/listings.module --routing --route=add-listing
nx g @nrwl/angular:module --name=edit-listing    --project=dashboard-listings --module=/listings.module --routing --route=edit
nx g @nrwl/angular:module --name=listing         --project=dashboard-listings --module=/listings.module --routing --route=listing
```

For now, start by creating a single component in the new library.

- adds the component to the new library
- updates the `listings.module`
- creates a route

```ts
nx g @nrwl/angular:module --name=manage-listings --project=dashboard-listings --module=/listings.module --routing --route=manage
```

The output of the CLI command.

```ts
nx g @nrwl/angular:module --name=manage-listings --project=dashboard-listings --module=/listings.module --routing --route=manage

CREATE libs/dashboard/listings/src/lib/manage-listings/manage-listings-routing.module.ts
CREATE libs/dashboard/listings/src/lib/manage-listings/manage-listings.module.ts
CREATE libs/dashboard/listings/src/lib/manage-listings/manage-listings.component.css
CREATE libs/dashboard/listings/src/lib/manage-listings/manage-listings.component.html
CREATE libs/dashboard/listings/src/lib/manage-listings/manage-listings.component.spec.ts
CREATE libs/dashboard/listings/src/lib/manage-listings/manage-listings.component.ts
UPDATE libs/dashboard/listings/src/lib/listings.module.ts
```

- container module is updated `listing.module.ts`
- a route is added to the `forChild(..)` routes
  - targeting the new component/module
- the components within the library project also take advantage of lazy-loading

```ts
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { 
        path: 'manage', 
        loadChildren: () => import('./manage-listings/manage-listings.module').then(m => m.ManageListingsModule) }
      /* {path: '', pathMatch: 'full', component: InsertYourComponentHere} */
    ]),
  ],
})
export class ListingsModule {}
```

### ManageListings Component with Module

- using a component/module combination to allow for lazy-loading
  - Single-Component-Application-Module ([Lars Gyrup Brink Nielsen](https://twitter.com/LayZeeDK))
- the module contains a single route to the component

```txt
.
├── manage-listings-routing.module.ts
├── manage-listings.component.html
├── manage-listings.component.scss
├── manage-listings.component.spec.ts
├── manage-listings.component.ts
└── manage-listings.module.ts
```

### ManageListingsModule

```ts
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ManageListingsRoutingModule } from './manage-listings-routing.module';
import { ManageListingsComponent } from './manage-listings.component';

const routes: Routes = [
  { path: '', component: ManageListingsComponent }
];

@NgModule({
  declarations: [
    ManageListingsComponent
  ],
  imports: [
    CommonModule,
    ManageListingsRoutingModule,
    RouterModule.forChild(routes)
  ]
})
export class ManageListingsModule { }
```

### ManageListingsComponent

```ts
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ijs-london-manage-listings',
  templateUrl: './manage-listings.component.html',
  styleUrls: ['./manage-listings.component.css']
})
export class ManageListingsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
```

## Current HTML Template

The template to display a single listing.

```html
<!-- MANAGE LISTINGS FEATURE :: START -->
<h3>Manage Listings</h3>
<br>
<div class="row">
<div class="col-md-4">
    <div class="card card-product">
    <div class="card-header card-header-image" data-header-animation="true">
        <a href="#pablo">
        <img class="img" src="./assets/img/card-2.jpg">
        </a>
    </div>
    <div class="card-body">
        <div class="card-actions text-center">
        <button mat-raised-button type="button" class="btn btn-danger btn-link fix-broken-card">
            <i class="material-icons">build</i> Fix Header!
        </button>
        <button mat-raised-button type="button" class="btn btn-default btn-link" matTooltip="View"
            [matTooltipPosition]="'below'">
            <i class="material-icons">art_track</i>
        </button>
        <button mat-raised-button type="button" class="btn btn-success btn-link" matTooltip="Edit"
            [matTooltipPosition]="'below'">
            <i class="material-icons">edit</i>
        </button>
        <button mat-raised-button type="button" class="btn btn-danger btn-link" matTooltip="Remove"
            [matTooltipPosition]="'below'">
            <i class="material-icons">close</i>
        </button>
        </div>
        <h4 class="card-title">
        <a href="#pablo">Cozy 5 Stars Apartment</a>
        </h4>
        <div class="card-description">
        The place is close to Barceloneta Beach and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Barcelona.
        </div>
    </div>
    <div class="card-footer">
        <div class="price">
        <h4>$899/night</h4>
        </div>
        <div class="stats">
        <p class="card-category"><i class="material-icons">place</i> Barcelona,Spain</p>
        </div>
    </div>
    </div>
</div>
</div>
<!-- MANAGE LISTINGS FEATURE :: END -->
```

## Data for Listing

Create  a class for the display items.

```ts
export class Listing {
    image: string;
    title: string;
    description: string;
    priceInfo: string;
    location: string;
}
```

## Feature Library :: Manage Listings

![listings-feature-library](resources/listings-feature-library.png)