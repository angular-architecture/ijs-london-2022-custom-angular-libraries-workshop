# Create Angular Workspace

Use the Angular CLI to create a new "*workspace*".

> ng new angular-workspace --help --dry-run

```ts
ng new angular-workspace --help -d
arguments:
  name
    The name of the new workspace and initial project.

options:
  --collection (-c)
    A collection of schematics to use in generating the initial application.
  --commit 
    Initial git repository commit information.
  --create-application 
    Create a new initial application project in the 'src' folder of the new workspace. When false, creates an empty workspace with no initial application. You can then use the generate application command so that all applications are created in the projects folder.
  --defaults 
    Disable interactive input prompts for options with a default.
  --directory 
    The directory name to create the workspace in.
  --dry-run (-d)
    Run through and reports activity without writing out results.
  --force (-f)
    Force overwriting of existing files.
  --help 
    Shows a help message for this command in the console.
  --inline-style (-s)
    Include styles inline in the component TS file. By default, an external styles file is created and referenced in the component TypeScript file.
  --inline-template (-t)
    Include template inline in the component TS file. By default, an external template file is created and referenced in the component TypeScript file.
  --interactive 
    Enable interactive input prompts.
  --legacy-browsers 
    Add support for legacy browsers like Internet Explorer using differential loading.
  --minimal 
    Create a workspace without any testing frameworks. (Use for learning purposes only.)
  --new-project-root 
    The path where new projects will be created, relative to the new workspace root.
  --package-manager 
    The package manager used to install dependencies.
  --prefix (-p)
    The prefix to apply to generated selectors for the initial project.
  --routing 
    Generate a routing module for the initial project.
  --skip-git (-g)
    Do not initialize a git repository.
  --skip-install 
    Do not install dependency packages.
  --skip-tests (-S)
    Do not generate "spec.ts" test files for the new project.
  --strict 
    Creates a workspace with stricter type checking and stricter bundle budgets settings. This setting helps improve maintainability and catch bugs ahead of time. For more information, see https://angular.io/guide/strict-mode
  --style 
    The file extension or preprocessor to use for style files.
  --verbose (-v)
    Add more details to output logging.
  --view-encapsulation 
    The view encapsulation strategy to use in the initial project.
```

## New [Angular] Workspace

The Angular Workspace was introduced in May of 2018.

- introduced a monorepo
- allows multiple projects
- introduced the new ***library*** project type
- added the angular.json file to configure projects within the workspace
- added the `paths` to the TypeScript configuration file
  - mapping alias/name of a library project to the file system path (relative to the Workspace root)

> ng new angular-workspace --create-application=false --package-manager=yarn --prefix=ijs-london   

```ts
ng new angular-workspace --create-application=false --package-manager=yarn --prefix=ijs-london   

CREATE angular-workspace/README.md (1063 bytes)
CREATE angular-workspace/.editorconfig (274 bytes)
CREATE angular-workspace/.gitignore (604 bytes)
CREATE angular-workspace/angular.json (184 bytes)
CREATE angular-workspace/package.json (1032 bytes)
CREATE angular-workspace/tsconfig.json (783 bytes)
✔ Packages installed successfully.
    Directory is already under version control. Skipping initialization of git.
```