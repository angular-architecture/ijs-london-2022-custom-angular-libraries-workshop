# Generate [Angular] Application Project

> Branch : chapter-02/section-2/generate-new-application-project

Create new application project with Angular CLI.

- add project to the `projects` folder by default

```ts
ng g application ijs      
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS   [ https://sass-lang.com/documentation/syntax#scss                ]
CREATE projects/ijs/.browserslistrc (703 bytes)
CREATE projects/ijs/karma.conf.js (1424 bytes)
CREATE projects/ijs/tsconfig.app.json (295 bytes)
CREATE projects/ijs/tsconfig.spec.json (341 bytes)
CREATE projects/ijs/src/favicon.ico (948 bytes)
CREATE projects/ijs/src/index.html (289 bytes)
CREATE projects/ijs/src/main.ts (372 bytes)
CREATE projects/ijs/src/polyfills.ts (2820 bytes)
CREATE projects/ijs/src/styles.scss (80 bytes)
CREATE projects/ijs/src/test.ts (788 bytes)
CREATE projects/ijs/src/assets/.gitkeep (0 bytes)
CREATE projects/ijs/src/environments/environment.prod.ts (51 bytes)
CREATE projects/ijs/src/environments/environment.ts (658 bytes)
CREATE projects/ijs/src/app/app-routing.module.ts (245 bytes)
CREATE projects/ijs/src/app/app.module.ts (393 bytes)
CREATE projects/ijs/src/app/app.component.scss (0 bytes)
CREATE projects/ijs/src/app/app.component.html (24617 bytes)
CREATE projects/ijs/src/app/app.component.spec.ts (1064 bytes)
CREATE projects/ijs/src/app/app.component.ts (208 bytes)
UPDATE angular.json (3467 bytes)
UPDATE package.json (1081 bytes)
✔ Packages installed successfully.
```

Verify the application:

```ts
ng build
ng test
ng serve
```