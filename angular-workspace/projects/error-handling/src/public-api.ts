/*
 * Public API Surface of error-handling
 */

export { ErrorHandlingService } from './lib/error-handling.service';
export { ErrorHandlingModule } from './lib/error-handling.module';
export { ErrorHandlingOptions } from './lib/error-handling-options';
