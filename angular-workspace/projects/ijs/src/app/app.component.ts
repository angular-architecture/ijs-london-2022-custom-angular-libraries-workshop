import { Component } from '@angular/core';
import { ErrorHandlingService } from 'error-handling';
import { LoggingService } from 'logging';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ijs';

  constructor(
    private logger: LoggingService) {
    this.logger.log(`Preparing to load the application...`);
  }
}
