import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ErrorHandlingModule, ErrorHandlingService } from 'error-handling';
import { LoggingModule, LoggingService, DataDogOptions } from 'logging';

const loggingOptions: DataDogOptions = {
  applicationName: 'ijs-london',
  clientToken: 'pub905382b0fa24bc2f51efa742260ef110'
}
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ErrorHandlingModule.forRoot({
      applicationName: 'ijs-library-workshop'
    }),
    LoggingModule.forRoot(loggingOptions),
  ],
  providers: [
    ErrorHandlingService,
    LoggingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
