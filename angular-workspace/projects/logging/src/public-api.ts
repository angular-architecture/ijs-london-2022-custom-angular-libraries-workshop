/*
 * Public API Surface of logging
 */

export { LoggingService } from './lib/logging.service';
export { LoggingModule } from './lib/logging.module';
export { DataDogOptions } from './lib/datadog-options';
