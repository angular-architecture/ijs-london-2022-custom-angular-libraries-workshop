import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { LoggingService } from '@ijs-london/logging';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'ijs-london-my-app',
  templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {
  private _router: Subscription;

  constructor(
    private logger: LoggingService,
    private router: Router) {
  }

  ngOnInit() {
    this.logger.log(`Preparing to initialize the application component.`);
    this._router = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
      const body = document.getElementsByTagName('body')[0];
      const modalBackdrop = document.getElementsByClassName('modal-backdrop')[0];
      if (body.classList.contains('modal-open')) {
        body.classList.remove('modal-open');
        modalBackdrop.remove();
      }
    });
  }
}
