import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageListingsComponent } from './manage-listings.component';

const routes: Routes = [{ path: '', component: ManageListingsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageListingsRoutingModule { }
