import { NgModule, ModuleWithProviders } from '@angular/core';
import { ErrorHandlingOptions } from './error-handling-options';

@NgModule({
  declarations: [
  ],
  imports: [
  ],
  exports: [
  ]
})
export class ErrorHandlingModule {
  static forRoot(options: ErrorHandlingOptions): ModuleWithProviders<ErrorHandlingModule> {
    return {
      ngModule: ErrorHandlingModule,
      providers: [
        // add providers here;
        {
          provide: ErrorHandlingOptions,
          useValue: options
        }
      ]
    }
  }
}
