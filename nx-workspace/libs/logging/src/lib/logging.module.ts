import { NgModule, ModuleWithProviders } from '@angular/core';
import { DataDogOptions } from './datadog-options';

@NgModule({
  declarations: [
  ],
  imports: [
  ],
  exports: [
  ]
})
export class LoggingModule {
  static forRoot(options: DataDogOptions): ModuleWithProviders<LoggingModule> {
    return {
      ngModule: LoggingModule,
      providers: [
        {
          provide: DataDogOptions,
          useValue: options
        }
      ]
    }
  }
}
