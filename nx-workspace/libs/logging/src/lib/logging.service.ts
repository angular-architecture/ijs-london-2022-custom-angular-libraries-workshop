import { Injectable } from '@angular/core';
import { datadogLogs } from '@datadog/browser-logs';
import { DataDogOptions } from './datadog-options';

@Injectable({
  providedIn: 'root'
})
export class LoggingService {

  constructor(private loggerOptions: DataDogOptions) {
    this.initializeLogger();
  }

  private initializeLogger() {
    datadogLogs.init({
      clientToken: this.loggerOptions.clientToken,
      site: 'datadoghq.com',
      forwardErrorsToLogs: true,
      sampleRate: 100
    });

    datadogLogs.logger.log(`Initialized DataDog logger.`, { application: this.loggerOptions.applicationName }, 'info');
  }

  log(message: string) {
    datadogLogs.logger.info(message);
  }

  error(message: string) {
    datadogLogs.logger.error(message);
  }
}
