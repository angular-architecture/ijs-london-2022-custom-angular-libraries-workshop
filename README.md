# IJS (International JavaScript Conference) London 2022: Custom Angular Libraries Workshop

> Repository/README at: https://bit.ly/ijs-london-libs

## Part 1: Getting started

Please review the "Getting Started" section for the workshop.

- [Getting Started](./workshop/getting-started.md)

### Git Code Repository

The course source code repository is located in Gitlab.

> https://gitlab.com/angular-architecture/ijs-london-2022-custom-angular-libraries-workshop

Each section of the course will be incrementally implemented in a Git branch. There will be (2) implementations of the library projects.

1. Angular (default) Workspace
2. Nx Workspace

> See all branches on Gitlab: https://gitlab.com/angular-architecture/ijs-london-2022-custom-angular-libraries-workshop/-/branches?state=all&sort=name_asc

### Basic Library Management with Angular Workspace

- part-02/section-1/create-angular-workspace
- part-02/section-2/generate-new-application-project
- part-02/section-2/generate-new-library-project
- part-02/section-3/implement-error-handling-library
- part-03/section-1/use-error-handling-library
- part-04/section-1/create-logging-library-project
- part-04/section-2/implement-logging-service
- part-04/section-3/import-logging-module-in-application
- part-04/section-5/update-error-handling-service-to-log
- Create a DataDog account ([see Instructions in PDF](./create-datadog-account.pdf))

### Advanced Library Management with Nx Workspace

- part-05/section-1/create-empty-nx-workspace-with-options
- part-05/section-2/add-application-project-with-options
- part-05/section-3/add-library-project-with-options
- part-06/section-1/review-dashboard-demo-application
- part-07/section-1/update-library-projects
- part-07/section-2/feature-library-project
- part-08/section-1/create-types-library

## Part 2: Angular Workspace

### Create Angular Workspace

- [Create Angular Workspace](./workshop/create-angular-workspace.md)
- Final Branch: `part-02/section-1/create-angular-workspace`

### Generate New Application Project

- [Generate Application](./workshop/generate-angular-application-project.md)
- Final Branch: `part-02/section-2/generate-new-application-project`

### Generate New Library Project

- [Generate Library](./workshop/generate-angular-library-project.md)
- Final Branch: part-02/section-2/generate-new-library-project

### Implement Error Handling

- [Implement Error Handling library](./workshop/implement-error-handling-library.md)
- Final Branch: `part-02/section-3/implement-error-handling-library`

## Part 3: Using Error Handling Library

- [Use Error Handling](./workshop/use-error-handling-library.md)
- Final Branch: `part-03/section-1/use-error-handling-library`

## Part 4: Logging Library

### Create Logging Library Project

- [Create Logging Library](./workshop/create-logging-library.md)
- Final Branch: `part-04/section-1/create-logging-library-project`

### DataDog

- Create a DataDog account

### Implement Logging Service

- [Implement Logging Service](./workshop/implement-logging-service.md)
- Final Branch: `part-04/section-2/implement-logging-service`

### Import Logging Module In Application

- [Import/Use logging module](./workshop/implement-logging-service.md#configure-the-logging-service-for-the-application)
- Final Branch: `part-04/section-3/import-logging-module-in-application`

### Update Error Handling Service To Log

- [Update Error Handling Service](./workshop/implement-logging-service.md#update-errorhandling-with-logging)
- Final Branch: `part-04/section-5/update-error-handling-service-to-log`

## Part X: Publishing Angular Libraries

> See: [Publishing Angular Libraries](./workshop/publish-angular-libraries.md)

- [Create NPM account](./workshop/publish-angular-libraries.md#npm-account)
- [Scope Names](./workshop/publish-angular-libraries.md#scope-name)
- [Create Library](./workshop/publish-angular-libraries.md#create-library-step-1)
- [NPM Login](./workshop/publish-angular-libraries.md#npm-login )
- [NPM Publish](./workshop/publish-angular-libraries.md#npm-publish-step-3)
- [Consuming Published Packages](./workshop/publish-angular-libraries.md#consuming-published-packages)

## Part 5: Nx Workspace

### Create Empty Nx Workspace With Options

- [Create Nx Workspace](./workshop/nx-workspace.md)
- Final Branch: `part-05/section-1/create-empty-nx-workspace-with-options`

### Add Application Project With Options

- [Add Application project](./workshop/nx-workspace.md#add-angular-application)
- Final Branch: `part-05/section-2/add-application-project-with-options`

### Add Library Project With Options

- [Add Library project](./workshop/nx-workspace.md#add-angular-library-project)
- Final Branch: `part-05/section-3/add-library-project-with-options`

## Part 6: Dashboard Demo Application

review dashboard demo application

- [Review Dashboard application](./workshop//review-dashboard-application.md)
- Final Branch: `part-06/section-1/review-dashboard-demo-application`

## Part 7: Library Projects

### Update Library Projects

- [Update library projects](./workshop/update-library-projects.md)
- Final Branch: `part-07/section-1/update-library-projects`

### Feature Library Project

- [Feature Library Project Type](./workshop/feature-library-project.md)
- Final Branch: `part-07/section-2/feature-library-project`

## Part 8: Types Library

### Create Types Library

- [Types Library for Models/Classes](./workshop/add-types-library-project.md)
- Final Branch: `part-08/section-1/create-types-library`

## Resources

- See: [Angular Library Resources](/workshop/resources.md)